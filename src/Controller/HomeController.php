<?php

namespace MicroCMS\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use MicroCMS\Domain\Comment;
use MicroCMS\Form\Type\CommentType;
use MicroCMS\Domain\User;
use MicroCMS\Form\Type\RegisterType;

class HomeController {

    /**
     * Home page controller.
     *
     * @param Application $app Silex application
     */
    public function indexAction(Application $app) {
        $articles = $app['dao.article']->findLast();
        return $app['twig']->render('index.html.twig', array('articles' => $articles));
    }
    
	public function chapterAction(Application $app) {
        $articles = $app['dao.article']->findAll();
        return $app['twig']->render('chapter.html.twig', array('articles' => $articles));
    }
	
    /**
     * Article details controller.
     *
     * @param integer $id Article id
     * @param Request $request Incoming request
     * @param Application $app Silex application
     */

    public function articleAction($id, Request $request, Application $app) {
        $article = $app['dao.article']->find($id);
        $commentFormView = null;
        $comment = new Comment();
        $comment->setArticle($article);
        $date = date('Y-m-d H:i:s');
        $comment->setDate($date);
        $commentForm = $app['form.factory']->create(CommentType::class, $comment);
        $commentForm->handleRequest($request);
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $app['dao.comment']->save($comment);
            $app['session']->getFlashBag()->add('success', 'Votre commentaire a bien été ajouté.');
        }
        $commentFormView = $commentForm->createView();
        $comments = $app['dao.comment']->findAllByArticle($id);
        return $app['twig']->render('article.html.twig', array(
            'article' => $article,
            'comments' => $comments,
            'commentForm' => $commentFormView));
    }

    /**
     * Comment response controller
     *
     * @param Request $request Incoming request
     * @param Application $app Silex application
     */

    public function respondCommentAction($id, Request $request, Application $app)
    {
        $date = date('Y-m-d H:i:s');
        $signalNULL = NULL;
        $comment = new Comment();
        $comment->setAuthor($request->get("authorMessage"));
        $comment->setContent($request->get("commentMessage"));
        if (empty($comment->getAuthor())) {
            return $app->redirect($app['url_generator']->generate('article', array(
                'id' => $id)));
        }
        else if (empty($comment->getContent())) {
            return $app->redirect($app['url_generator']->generate('article', array(
                'id' => $id)));
        }
        $comment->setDate($date);
        $comment->setSignal($signalNULL);
        $comment->setParents($request->get("commentaireParent"));
        $article = $app['dao.article']->find($id);
        $comment->setArticle($article);
        $app['dao.comment']->save($comment);
        $app['session']->getFlashBag()->add('success', 'Votre commentaire a bien été ajouté.');
        return $app->redirect($app['url_generator']->generate('article', array(
            'id' => $id)));
    }
    /**
     * User register controller.(user)
     *
     * @param Request $request Incoming request
     * @param Application $app Silex application
     */
	
    public function registerAction(Request $request, Application $app) {
        $user = new User();
        $userForm = $app['form.factory']->create(RegisterType::class, $user);
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            // generate a random salt value
            $salt = substr(md5(time()), 0, 23);
            $user->setSalt($salt);
            $plainPassword = $user->getPassword();
            // find the default encoder
            $encoder = $app['security.encoder.bcrypt'];
            // compute the encoded password
            $password = $encoder->encodePassword($plainPassword, $user->getSalt());
            $user->setPassword($password); 
            $app['dao.user']->save($user);
            $app['session']->getFlashBag()->add('success', 'Utilisateur créé avec succès');
        }
        return $app['twig']->render('register.html.twig', array(
            'title' => 'Inscription',
            'userForm' => $userForm->createView()));
    }
	
    /**
     * User login controller.
     *
     * @param Request $request Incoming request
     * @param Application $app Silex application
     */
    public function loginAction(Request $request, Application $app) {
        return $app['twig']->render('login.html.twig', array(
            'error'         => $app['security.last_error']($request),
            'last_username' => $app['session']->get('_security.last_username'),
        ));
    }
}
