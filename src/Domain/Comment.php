<?php

namespace MicroCMS\Domain;

class Comment 
{
    /**
     * Comment id.
     *
     * @var integer
     */
    private $id;
	
	/**
     * Comment parent.
     *
     * @var integer
     */
	private $parents;
	
	/**
     * Comment signalisation.
     *
     * @var integer
     */
	private $signal;

    /**
     * Comment author.
     *
     * @var \MicroCMS\Domain\User
     */
    private $author;

    /**
     * Comment content.
     *
     * @var integer
     */
    private $content;

    /**
     * Associated article.
     *
     * @var \MicroCMS\Domain\Article
     */
    private $article;

    /**
     * Comment date.
     *
     * @var \MicroCMS\Domain\Article
     */
    private $date;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }
	
	public function getParents() {
        return $this->parents;
    }

    public function setParents($parents) {
        $this->parents = $parents;
        return $this;
    }
	
	public function getSignal() {
        return $this->signal;
    }

    public function setSignal($signal) {
        $this->signal = $signal;
        return $this;
    }

    public function getAuthor() {
        return $this->author;
    }

    public function setAuthor($author) {
        $this->author = $author;
        return $this;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
        return $this;
    }

    public function getArticle() {
        return $this->article;
    }

    public function setArticle(Article $article) {
        $this->article = $article;
        return $this;
    }

    public function getDate() {
        return $this->date;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }
}
