<?php

// Gère l'affichage du formulaire de création d'un nouvel utilisateur.

namespace MicroCMS\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array (
                'label'       => 'Pseudo',
            ))
			->add('email', EmailType::class)
            ->add('password', RepeatedType::class, array(
                 'type'            => PasswordType::class,
                 'invalid_message' => 'Les mots de passe ne correspondent pas',
                 'options'         => array('required' => true),
                 'first_options'   => array('label' => 'Mot de passe'),
                 'second_options'  => array('label' => 'Répéter le mot de passe'),
             ))
            ->add('role', HiddenType::class, array(
                'data' => 'ROLE_ADMIN',
                    ));
    }

    public function getName()
    {
        return 'user';
    }
}
