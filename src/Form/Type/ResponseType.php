<?php

namespace MicroCMS\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints as Assert;

class ResponseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)

    {
        $builder->add('author', TextType::class,[
            'label'       => 'Votre pseudo',
            'required'    => true,
            'constraints' => new Assert\NotBlank(),
        ])
            ->add('content', TextareaType::class,[
            'label'       => 'Votre commentaire',
            'required'    => true,
            'constraints' => new Assert\NotBlank(),
        ]);
    }

    public function getName()
    {
        return 'response';
    }
}
